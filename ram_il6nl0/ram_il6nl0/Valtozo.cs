﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ram_il6nl0
{
     abstract class Valtozo
    {
        public string Name { get; set; }
        public int AppID { get; set; }
        public abstract int Size { get; }


    }

    class Egesz : Valtozo
    {
        public int Value { get; set; }

        public override int Size
        {
             get { return 4; }
        }
    }

    class Bajt : Valtozo
    {
        public byte Value { get; set; }

        public override int Size
        {
            get { return 1; }
        }

    }

    class Karakter : Valtozo
    {
        public char Value { get; set; }

        public override int Size
        {
            get { return 2; }
        }

    }

    class Szoveg : Valtozo
    {
        public string Value { get; set; }

        public override int Size
        {
            get { return Value.Length*2; }
        }

    }

    class Tort : Valtozo
    {
        public double Value { get; set; }

        public override int Size
        {
            get { return 4; }
        }

    }


}
