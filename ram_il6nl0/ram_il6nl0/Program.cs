﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace ram_il6nl0
{
    class Program
    {
        static void Main(string[] args)
        {
            //BubiHivo();
            Teszt();
            //MaxTest();
            Console.WriteLine("lefutott a teszt");
            Console.ReadLine();
        }

        private static void MaxTest()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            RAM ram = new RAM();
            for (int i = 0; i < 10; i++)
            {
                ram.SetVariable(new Egesz() { Name = i.ToString(), Value = i, AppID = 0 });
            }
            ram.Clean();
            sw.Stop();
            Console.WriteLine(sw.Elapsed);
        }

        private static void BubiHivo()
        {
            RAM ram = new RAM();
            List<Egesz> egeszLista = new List<Egesz>();
            string[] s;
            using (StreamReader sr = new StreamReader("init.txt"))
            {
                sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    s = sr.ReadLine().Split('\t');
                    egeszLista.Add( new Egesz() { Name = s[0], Value = int.Parse(s[1]), AppID = int.Parse(s[2]) } );
                    try
                    {
                        ram.SetVariable(egeszLista.Last());
                    }
                    catch (OutOfMemoryException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            JavBubiRendezes(egeszLista.ToArray(), ram);
        }

        static void Teszt()
        {
            RAM ram = new RAM();
            string[] s;
            using (StreamReader sr = new StreamReader("init.txt"))
            {
                sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    s = sr.ReadLine().Split('\t');
                    try
                    { 
                        ram.SetVariable(new Egesz() { Name = s[0], Value = int.Parse(s[1]), AppID = int.Parse(s[2]) });
                    }
                    catch (OutOfMemoryException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            try
            {
                ram.SetVariable(new Egesz() { Name = "szám1", Value = 1, AppID = 1});
            }
            catch (NameAlreadyExistsException e)
            {
                Console.WriteLine(e.Message);
            }
           // ram.ListRAM();
            try
            {
                ram.DelVariable("szám1");
                ram.DelVariable("szám2");
                ram.DelVariable("szám4");
                ram.DelVariable("valami");
            }
            catch (NotInTheMemoryException e)
            {
                Console.WriteLine(e.Message);
            }
            ram.Clean();
        //    ram.ListRAM();
            Console.WriteLine(ram.GetVariable("szám5").Name);

        }

        static void JavBubiRendezes(Egesz[] x, RAM ram)
        {
            foreach (Egesz item in x) ram.DelVariable(item.Name);

            int i = x.Length - 1;
            int idx;
            while (i >= 1)
            {
                idx = 0;
                for (int j = 0; j < i - 1; j++)
                {
                    if (x[j].Value > x[j + 1].Value)
                    {
                        Egesz temp = x[j + 1];
                        x[j + 1] = x[j];
                        x[j] = temp;
                        idx = j;
                    }
                }
                i = idx;
            }
            foreach (Egesz item in x)
            {
                try
                {
                    ram.SetVariable(item);
                }
                catch (OutOfMemoryException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch(NameAlreadyExistsException e2)
                {
                    Console.WriteLine(e2.Message);
                }
            }
            ram.Clean();
        }

    /*    static void JavBubiRendezes(Egesz[] x, RAM ram)
        {
            foreach (Egesz item in x)     ram.DelVariable(item.Name);

            int i = x.Length - 1;
            int idx;
            while (i >= 1)
            {
                idx = 0;
                for (int j = 0; j < i-1; j++)
                {
                    if(x[j].Value > x[j + 1].Value)
                    {
                        Egesz temp = x[j + 1];
                        x[j + 1] = x[j];
                        x[j] = temp;
                        idx = j;
                    }
                }
                i = idx;
            }
            foreach (Egesz item in x)
            {
                ram.SetVariable(item);
            }
            ram.Clean();
        }*/
    }
}
