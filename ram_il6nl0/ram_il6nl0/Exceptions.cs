﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ram_il6nl0
{
    class NotInTheMemoryException : Exception
    {
        public NotInTheMemoryException(string message) : base(message)
        {

        }
    }

    class OutOfMemoryException : Exception
    {
        public OutOfMemoryException(string message) : base(message)
        {

        }
    }

    class NameAlreadyExistsException : Exception
    {
        public NameAlreadyExistsException(string message) : base(message)
        {

        }
    }
}
