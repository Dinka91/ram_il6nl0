﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ram_il6nl0
{
    public enum types { egesz, bajt, karakter, szoveg, tort};
    class RAM
    {
        public void ListRAM()
        {

            for (int i = 1; i < _ram.Length + 1; i++)
            {
                Console.Write(_ram[i - 1] + " ");
                if (i % 4 == 0)
                    Console.WriteLine();
            }
        }

        public class ListaElem
        {
            public string _name;
            public types _type;
            public int _loc;
            public int _endloc;
            public ListaElem _next;
            public int Weigth { get; set; }
            public int AppID { get; set; }
            public int Size
            {
                get; set;
            }


        }
        
        ListaElem _head;
        byte[] _ram = new byte[1000000];

        public ListaElem this [int index]
        {
            get
            {
                int db = 0;
                ListaElem e = _head;
                while (e != null && db < index)
                {
                    e = e._next;
                    db++;
                }
                if (e != null)
                    return e;
                else
                    throw new ArgumentException("Nincs ilyen elem");
            }
        }

        public int VariableCount
        {
            get
            {
                int N = 0;
                ListaElem e = _head;
                while (e != null)
                {
                    e = e._next;
                    N++;
                }
                return N;
            }
        }


        public void SetVariable(Valtozo v)
        {
            ListaElem p = _head;
            while (p != null && p._name != v.Name)
                p = p._next;
            if(p != null)
                throw new NameAlreadyExistsException("Már létezik ilyen nevű elem a memóriában.");
            ListaElem uj = new ListaElem();
            uj._name = v.Name;
            byte[] varInBytes;
            if (v is Egesz)
            {
                uj._type = types.egesz;
                varInBytes = BitConverter.GetBytes(((Egesz)v).Value);
            }
            else if (v is Bajt)
            {
                uj._type = types.bajt;
                varInBytes = new byte[] { ((Bajt)v).Value };
            }
            else if (v is Karakter)
            {
                uj._type = types.karakter;
                varInBytes = BitConverter.GetBytes(((Karakter)v).Value);
            }
            else if (v is Szoveg)
            {
                uj._type = types.szoveg;
                varInBytes = Encoding.Default.GetBytes(((Szoveg)v).Value);
            }
            else if (v is Tort)
            {
                uj._type = types.tort;
                varInBytes = BitConverter.GetBytes(((Tort)v).Value);
            }
            else
                throw new NotSupportedException();
            if (_head == null)
            {
                uj._loc = 0;
                if (varInBytes.Length > _ram.Length)
                    throw new OutOfMemoryException("Elfogyott a memória.");
                for (int i = 0; i < varInBytes.Length; i++)
                {
                    _ram[i] = varInBytes[i];
                }
                uj._endloc = varInBytes.Length;
                uj.Size = varInBytes.Length;
                uj.AppID = v.AppID;
                _head = uj;
            }
            else
            {
                p = _head;
                while (p._next != null)
                    p = p._next;
                if(p._endloc + varInBytes.Length > _ram.Length)
                    throw new OutOfMemoryException("Elfogyott a memória.");
                p._next = uj;
                uj._loc = p._endloc;
                for (int i = 0; i < varInBytes.Length; i++)
                {
                    _ram[i + uj._loc] = varInBytes[i];
                }
                uj.Size = varInBytes.Length;
                uj._endloc = uj._loc + varInBytes.Length;
                uj.AppID = v.AppID;
            }
        }

        public Valtozo GetVariable(string name)
        {
            ListaElem p = _head;
            while (p != null && p._name != name)
            {
                p = p._next;
            }
            if (p != null)
            {
                if (p._type == types.egesz)
                    return new Egesz() { Name = name, Value = BitConverter.ToInt32(_ram, p._loc), AppID=p.AppID };
                else if (p._type == types.bajt)
                    return new Bajt() { Name = name, Value = _ram[p._loc], AppID = p.AppID };
                else if (p._type == types.karakter)
                    return new Karakter() { Name = name, Value = BitConverter.ToChar(_ram, p._loc), AppID = p.AppID };
                else if (p._type == types.szoveg)
                    return new Szoveg() { Name = name, Value = Encoding.Default.GetString(_ram, p._loc, p._endloc - p._loc), AppID = p.AppID };
                else if (p._type == types.tort)
                    return new Tort() { Name = name, Value = BitConverter.ToDouble(_ram, p._loc), AppID = p.AppID };
            }
            throw new NotInTheMemoryException("Nincs ilyen nevű elem a memóriában.");
        }

        public void DelVariable(string name)
        {
            if (_head != null)
            {
                if (_head._name == name)
                    _head = _head._next;
                else
                {
                    ListaElem p = _head._next;
                    ListaElem e = _head;
                    while (p != null && p._name != name)
                    {
                        e = p;
                        p = p._next;
                    }
                    if (p != null)
                    {
                        e._next = p._next;
                    }
                    if(p == null)
                        throw new NotInTheMemoryException("Nincs ilyen nevű elem a memóriában.");
                }
            }
        }



        //_________________________________________________CLEAN___________________________________________________________________________

        static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });
            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(o => !t.Contains(o)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }

        ListaElem[] GetNewRamList()
        {
            ListaElem e = _head;
            List<ListaElem> listaElemList = new List<ListaElem>();
            while (e != null)
            {
                listaElemList.Add(e);
                e = e._next;
            }
            return listaElemList.ToArray();
        }

        public void Clean()
        {
            ListaElem[] eredeti = GetNewRamList();
            ListaElem[] opt = new ListaElem[eredeti.Length];
            List<int> appIdCheckList = new List<int>();
            int optint = int.MaxValue;
            int aktoptint, count;
            if (eredeti.Length == 0)
                throw new InvalidProgramException();
            IEnumerable<IEnumerable<ListaElem>> permuList = GetPermutations<ListaElem>((IEnumerable<ListaElem>)eredeti, eredeti.Length);
            foreach (IEnumerable<ListaElem> item in permuList)
            {
                aktoptint = 0;
                appIdCheckList.Clear();
                ListaElem[] array = item.ToArray();
                array= LocReCalculate(array);
                foreach (ListaElem l in array)
                {
                    count = 0;
                    while (eredeti[count]._name != l._name)
                        count++;
                   if (eredeti[count]._loc != l._loc)
                    {
                        if (!appIdCheckList.Contains(l.AppID))
                        {
                            aktoptint += 1000;
                            appIdCheckList.Add(l.AppID);
                        }                        
                        aktoptint += 5 + eredeti[count]._loc - l._loc;                       
                    }
                }
                if (optint > aktoptint)
                {
                    optint = aktoptint;
                    int j = 0;                           
                    foreach (ListaElem l in array)
                    {
                        opt[j] = l;
                        j++;
                        
                    }
                            
                }
            }
            byte[] oldRAM = _ram;
            for (int i = 0; i < opt.Length; i++)
            {
                int count2 = 0;
                while (opt[i]._name != eredeti[count2]._name)
                    count2++;
                for (int j = 0; j < opt[i].Size; j++)
                {
                    _ram[opt[i]._loc + j] = oldRAM[eredeti[count2]._loc + j];
                }
            }

            opt = LocReCalculate(opt);
            for (int i = opt.Last()._endloc; i < _ram.Length; i++)
            {
                _ram[i] = 0;
            }
        }

        private static ListaElem[] LocReCalculate(ListaElem[] opt)
        {
            ListaElem[] result = new ListaElem[opt.Length];
            int i = 0;
            foreach (ListaElem item in opt)
            {
                ListaElem t = new ListaElem()
                {
                    AppID = item.AppID,
                    Size = item.Size,
                    Weigth = item.Weigth,
                    _endloc = item._endloc,
                    _loc = item._loc,
                    _name = item._name,
                    _next = item._next,
                    _type = item._type                   
                };
                result[i] = t;
                i++;
            }
            int temp = 0;
            for (int j = 0; j < result.Length; j++)
            {
                result[j]._loc = temp;
                if (result[j]._type == types.bajt)
                {
                    result[j]._endloc = result[j]._loc + 1;
                    temp = result[j]._endloc;
                }
                else if (result[j]._type == types.egesz)
                {
                    result[j]._endloc = result[j]._loc + 4;
                    temp = result[j]._endloc;
                }
                else if (result[j]._type == types.karakter)
                {
                    result[j]._endloc = result[j]._loc + 2;
                    temp = result[j]._endloc;
                }
                else if (result[j]._type == types.szoveg)
                {
                    result[j]._endloc = result[j]._loc + result[j].Size;
                    temp = result[j]._endloc;
                }
                else if (result[j]._type == types.tort)
                {
                    result[j]._endloc = result[j]._loc + 8;
                    temp = result[j]._endloc;
                }
            }
            return result;
        }
    }
}
